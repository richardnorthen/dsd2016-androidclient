package com.dsd2016.androidclient.AndroidAPI;

public class ServerMessage {
    public String userId;
    public String picture;
    public Picture[] pictures;

    public ServerMessage() {}

    /** Create a ServerMessage object for use with the <i>train</i> service. */
    public static ServerMessage trainMessage(String userId, String[] base64, int[] pictureId) {
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.userId = userId;
        serverMessage.pictures = new Picture[base64.length];
        for (int i = 0; i < serverMessage.pictures.length; i++) {
            serverMessage.pictures[i].base64 = base64[i];
            serverMessage.pictures[i].pictureId = pictureId[i];
        }
        return serverMessage;
    }

    /** Create a ServerMessage object for use with the <i>login</i> service. */
    public static ServerMessage loginMessage(String userId, String picture) {
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.userId = userId;
        serverMessage.picture = picture;
        return serverMessage;
    }

    public static class Picture {
        public String base64;
        public int pictureId;
        public Picture() {}
    }
}