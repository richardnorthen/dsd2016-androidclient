package com.dsd2016.androidclient.AndroidAPI;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers("Content-Type: application/json")
    @POST("register")
    Call<ServerResponse> register();

    @Headers("Content-Type: application/json")
    @POST("train")
    Call<ServerResponse> train(@Body ServerMessage message);

    @Headers("Content-Type: application/json")
    @POST("login")
    Call<ServerResponse> login(@Body ServerMessage message);
}
