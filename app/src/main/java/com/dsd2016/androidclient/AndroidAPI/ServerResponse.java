package com.dsd2016.androidclient.AndroidAPI;

public class ServerResponse {
    public String userId;
    public Error[] errors;
    public boolean success;
    public ServerResponse() {}

    public static class Error {
        public int errorCode, pictureId;
        public String errorMessage;
        public Error() {}
    }
}
