package com.dsd2016.androidclient.AndroidAPI;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIHandler {
    public static APIService service;

    /** Create a set of methods used to interact with the AuthenticationServer and its services. */
    public static void setAPIService(String url, int timeout) {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(timeout, TimeUnit.MINUTES).writeTimeout(timeout, TimeUnit.MINUTES).readTimeout(timeout, TimeUnit.MINUTES).build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        service = retrofit.create(APIService.class);
    }
}