package com.dsd2016.androidclient.Activities;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;

import com.dsd2016.androidclient.R;

public class SettingsActivity extends AppCompatActivity {
    public static final String KEY_URL = "pref_key_url";
    public static final String KEY_TIMEOUT = "pref_key_timeout";
    public static final String KEY_USE_USERID = "pref_key_use_userid";
    public static final String KEY_SHOW_LOG = "pref_key_show_log";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // load the available Settings
            addPreferencesFromResource(R.xml.preferences);
        }
    }
}
