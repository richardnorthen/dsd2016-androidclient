package com.dsd2016.androidclient.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dsd2016.androidclient.R;

public class ResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        String message = intent.getStringExtra("message");
        boolean success = intent.getBooleanExtra("success", false);

        ImageView imageView = (ImageView) findViewById(R.id.imgResultIcon);
        TextView textView = (TextView) findViewById(R.id.txtResultMessage);

        if (success) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_check, null));
        } else {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_error, null));
        }
        textView.setText(message);
    }

    public void done(View view) {
        Intent intent = new Intent(ResultActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
