package com.dsd2016.androidclient.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.dsd2016.androidclient.AndroidAPI.APIHandler;
import com.dsd2016.androidclient.AndroidAPI.ServerMessage;
import com.dsd2016.androidclient.AndroidAPI.ServerResponse;
import com.dsd2016.androidclient.Models.DBHandler;
import com.dsd2016.androidclient.Models.Picture;
import com.dsd2016.androidclient.Models.User;
import com.dsd2016.androidclient.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private DBHandler database;
    private AutoCompleteTextView inputEmail;
    private EditText inputPassword;

    private String userId = "";
    private boolean useUserId = false;
    private String picturePath = "";
    public static int REQUEST_TAKE_PHOTO = 1;

    private ProgressDialog progress;
    private EditText logger;
    private boolean showLog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // create database handler
        database = new DBHandler(this);
        // start API service
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String url = sharedPreferences.getString(SettingsActivity.KEY_URL, getString(R.string.api_url));
        int timeout = Integer.parseInt(sharedPreferences.getString(SettingsActivity.KEY_TIMEOUT, getString(R.string.api_timeout)));
        APIHandler.setAPIService(url, timeout);
        // open gui components
        inputEmail = (AutoCompleteTextView) findViewById(R.id.inputLoginEmail);
        inputPassword = (EditText) findViewById(R.id.inputLoginPassword);
        // check whether the USE_USERID preference was enabled
        useUserId = sharedPreferences.getBoolean(SettingsActivity.KEY_USE_USERID, false);
        if (useUserId) {
            inputEmail.setHint("UserID");
            inputPassword.setVisibility(View.GONE);
        }
    }

    /**
     * Performed when the Login button is clicked.
     * Checks that the account details are correct then requests a picture from the user.
     * @param view View
     */
    public void login(View view) {
        // check useUserId flag
        if (useUserId) {
            userId = inputEmail.getText().toString().trim();
            takePicture();
            return;
        }

        // reset errors and grab input
        inputEmail.setError(null);
        inputPassword.setError(null);
        final String email = inputEmail.getText().toString().trim();
        final String password = inputPassword.getText().toString().trim();

        // TODO improve validation
        View focusView = null;
        if (email.isEmpty()) {
            inputEmail.setError(getString(R.string.empty_field));
            focusView = inputEmail;
        } else if (!email.contains("@")) {
            inputEmail.setError(getString(R.string.invalid_email));
            focusView = inputEmail;
        }
        if (password.length() < 4) {
            inputPassword.setError(getString(R.string.invalid_password));
            focusView = inputPassword;
        }
        if (focusView != null) {
            focusView.requestFocus();
        } else {
            // check if account exists
            User user = database.getUser(email);
            if (user == null) {
                // email not registered
                inputEmail.setError(getString(R.string.not_registered));
                inputEmail.requestFocus();
            } else {
                // check that the password was correct
                boolean isCorrect = user.getPassword().equals(password);
                if (isCorrect) {
                    // take the picture and attempt to authenticate
                    userId = database.getUser(email).getUserId();
                    takePicture();
                } else {
                    // incorrect password
                    inputPassword.setError(getString(R.string.bad_login));
                    inputPassword.requestFocus();
                }
            }
        }
    }

    /** Allows the user to take a picture of themselves for logging in. */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            // create a file to store the picture
            File picture = null;
            try {
                picture = createPictureFile();
            } catch (IOException ex) {
                // TODO createFile error
            }
            if (picture != null) {
                // grab a picture from the user
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(picture));
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /** Creates a unique, empty file to store a picture into. */
    private File createPictureFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String pictureName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File picture = File.createTempFile(pictureName, ".jpg", storageDir);
        // keep a reference to the picture
        picturePath = picture.getAbsolutePath();
        return picture;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if a picture is successfully taken, attempt to authenticate the user
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            // display progress to user
            progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.progress_encoding));
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.setIndeterminate(true);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
            // build and send ServerMessage
            Picture picture = new Picture(picturePath);
            final ServerMessage serverMessage = ServerMessage.loginMessage(userId, picture.asBase64());
            // call the API login service
            progress.setMessage(getString(R.string.progress_sending));
            Call<ServerResponse> call = APIHandler.service.login(serverMessage);
            progress.setMessage(getString(R.string.progress_waiting));
            call.enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    progress.dismiss();
                    ServerResponse serverResponse = response.body();
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    if (serverResponse.success) {
                        // notify the user that the account was successfully authenticated
                        builder.setTitle("Success").setMessage(getString(R.string.success_login)).setPositiveButton("OK", null);
                        AlertDialog alert = builder.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.show();
                        // return to the MainActivity
                        finish();
                    } else {
                        // notify the user that the authentication was unsuccessful
                        builder.setTitle("Failed").setMessage(getString(R.string.failed_login)).setPositiveButton("OK", null);
                        AlertDialog alert = builder.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.show();
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    progress.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this).setTitle("Error").setMessage(getString(R.string.no_response)).setPositiveButton("OK", null);
                    AlertDialog alert = builder.create();
                    alert.setCanceledOnTouchOutside(false);
                    alert.show();
                }
            });
        }
    }
}