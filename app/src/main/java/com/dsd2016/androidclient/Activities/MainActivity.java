package com.dsd2016.androidclient.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.dsd2016.androidclient.R;

public class MainActivity extends AppCompatActivity {
    private static boolean APP_START = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // load default preferences when starting app
        if (APP_START) {
            APP_START = false;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().clear().apply();
            PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // add the settings button to the window
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.button_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.button_settings:
                // open the SettingsActivity when the Settings button is clicked
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    /**
     * Performed when the Register button is clicked and opens RegisterActivity.
     * @param view View
     */
    public void register(View view) {
        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    /**
     * Performed when the Login button is clicked and opens LoginActivity.
     * @param view View
     */
    public void login(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}