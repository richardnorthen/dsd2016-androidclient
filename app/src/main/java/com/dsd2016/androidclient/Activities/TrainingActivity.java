package com.dsd2016.androidclient.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.dsd2016.androidclient.AndroidAPI.APIHandler;
import com.dsd2016.androidclient.AndroidAPI.ServerMessage;
import com.dsd2016.androidclient.AndroidAPI.ServerResponse;
import com.dsd2016.androidclient.Models.DBHandler;
import com.dsd2016.androidclient.Models.Picture;
import com.dsd2016.androidclient.Models.PictureAdapter;
import com.dsd2016.androidclient.Models.User;
import com.dsd2016.androidclient.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrainingActivity extends AppCompatActivity {
    private User user;
    private DBHandler database;

    private String lastPicturePath = "";
    private static int REQUEST_TAKE_PHOTO = 1;

    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);
        // create database handler
        database = new DBHandler(this);
        // use extras to get User from database
        Intent intent = getIntent();
        user = database.getUser(intent.getStringExtra("email"));
        System.out.println(user.getName());
        // start API service
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String url = sharedPreferences.getString(SettingsActivity.KEY_URL, getString(R.string.api_url));
        int timeout = Integer.parseInt(sharedPreferences.getString(SettingsActivity.KEY_TIMEOUT, getString(R.string.api_timeout)));
        APIHandler.setAPIService(url, timeout);
        // set picture list for training
        ListView listPictures = (ListView) findViewById(R.id.listPictures);
        listPictures.setAdapter(new PictureAdapter(this, user.getPictures()));
    }

    /**
     * Performed when the Train button is clicked. Encodes the images and sends a <i>train</i> call to the API.
     * If any errors and returned, the user is notified and any problematic pictures are deleted.
     * @param view View
     */
    public void train(View view) {
        // display progress to user
        progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.progress_sending));
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        // build and send ServerMessage
        int size = user.getPictures().size();
        String[] base64 = new String[size];
        int[] pictureId = new int[size];
        for (int i = 0; i < size; i++) {
            Picture picture = user.getPictures().get(i);
            base64[i] = picture.asBase64();
            pictureId[i] = picture.getPictureId();
        }
        ServerMessage serverMessage = ServerMessage.trainMessage(user.getUserId(), base64, pictureId);
        // call the API train service
        progress.setMessage(getString(R.string.progress_sending));
        Call<ServerResponse> call = APIHandler.service.train(serverMessage);
        progress.setMessage(getString(R.string.progress_waiting));
        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                progress.dismiss();
                ServerResponse serverResponse = response.body();
                AlertDialog.Builder builder = new AlertDialog.Builder(TrainingActivity.this);
                if (serverResponse.success) {
                    // notify the user that the account was successfully trained
                    builder.setTitle("Success").setMessage(getString(R.string.success_train)).setPositiveButton("OK", null);
                    AlertDialog alert = builder.create();
                    alert.setCanceledOnTouchOutside(false);
                    alert.show();
                    // return to the MainActivity
                    Intent intent = new Intent(TrainingActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    // notify the user of all errors, and remove any relevant pictures
                    String message = "";
                    if (serverResponse.errors != null) {
                        for (int i = 0; i < serverResponse.errors.length; i++) {
                            ServerResponse.Error error = serverResponse.errors[i];
                            message += "Error: " + error.errorMessage + " (code " + error.errorCode;
                            // remove picture
                            if (error.errorCode == 1 || error.errorCode == 2 || error.errorCode == 3 || error.errorCode == 4) {
                                user.removePicture(error.pictureId);
                                message += ", picture " + error.pictureId + " removed";
                            }
                            message += ")\n";
                        }
                    }
                    builder.setTitle("Error").setMessage(message).setPositiveButton("OK", null);
                    AlertDialog alert = builder.create();
                    alert.setCanceledOnTouchOutside(false);
                    alert.show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                progress.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(TrainingActivity.this).setTitle("Error").setMessage(getString(R.string.no_response)).setPositiveButton("OK", null);
                AlertDialog alert = builder.create();
                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });
    }

    /**
     * Performed when the Camera button is clicked. Allows the user to take a picture of themselves for training.
     * @param view View
     */
    public void takePicture(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            // create a file to store the picture
            File picture = null;
            try {
                picture = createPictureFile();
            } catch (IOException ex) {
                // TODO createFile error
            }
            if (picture != null) {
                // grab a picture from the user
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(picture));
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /** Creates a unique, empty file to store a picture into. */
    private File createPictureFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String pictureName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File picture = File.createTempFile(pictureName, ".jpg", storageDir);
        // keep a reference to the picture
        lastPicturePath = picture.getAbsolutePath();
        return picture;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if a picture is successfully taken, add it to User
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Picture picture = new Picture(lastPicturePath);
            user.getPictures().add(picture);
        }
    }
}
