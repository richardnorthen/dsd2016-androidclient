package com.dsd2016.androidclient.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.dsd2016.androidclient.AndroidAPI.APIHandler;
import com.dsd2016.androidclient.AndroidAPI.ServerResponse;
import com.dsd2016.androidclient.Models.DBHandler;
import com.dsd2016.androidclient.Models.User;
import com.dsd2016.androidclient.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private DBHandler database;
    private AutoCompleteTextView inputName;
    private AutoCompleteTextView inputEmail;
    private EditText inputPassword;

    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // create database handler
        database = new DBHandler(this);
        // start API service
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String url = sharedPreferences.getString(SettingsActivity.KEY_URL, getString(R.string.api_url));
        int timeout = Integer.parseInt(sharedPreferences.getString(SettingsActivity.KEY_TIMEOUT, getString(R.string.api_timeout)));
        APIHandler.setAPIService(url, timeout);
        // open gui components
        inputName = (AutoCompleteTextView) findViewById(R.id.inputRegisterName);
        inputEmail = (AutoCompleteTextView) findViewById(R.id.inputRegisterEmail);
        inputPassword = (EditText) findViewById(R.id.inputRegisterPassword);
    }

    /**
     * Performed when the Register button is clicked. Attempts to create a new User in the database, as long as:
     * <ul>
     *     <li>name and email are non-empty</li>
     *     <li>email contains an @ symbol</li>
     *     <li>password is at least 4 characters long</li>
     * </ul>
     * If a valid User is created, a <i>register</i> call to the API is made to get a UserID.
     *
     * @param view View
     */
    public void register(View view) {
        // reset errors and grab input
        inputName.setError(null);
        inputEmail.setError(null);
        inputPassword.setError(null);
        final String name = inputName.getText().toString().trim();
        final String email = inputEmail.getText().toString().trim();
        final String password = inputPassword.getText().toString();

        // TODO improve validation
        View focusView = null;
        if (name.isEmpty()) {
            inputName.setError(getString(R.string.empty_field));
            focusView = inputName;
        }
        if (email.isEmpty()) {
            inputEmail.setError(getString(R.string.empty_field));
            focusView = inputEmail;
        } else if (!email.contains("@")) {
            inputEmail.setError(getString(R.string.invalid_email));
            focusView = inputEmail;
        }
        if (password.length() < 4) {
            inputPassword.setError(getString(R.string.invalid_password));
            focusView = inputPassword;
        }
        if (focusView != null) {
            focusView.requestFocus();
        } else {
            // check if email already registered
            User userReceived = database.getUser(email);
            if (userReceived == null) {
                // display progress to user
                progress = new ProgressDialog(this);
                progress.setMessage(getString(R.string.progress_sending));
                progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progress.setIndeterminate(true);
                progress.setCanceledOnTouchOutside(false);
                progress.show();
                // call the API register service
                Call<ServerResponse> call = APIHandler.service.register();
                progress.setMessage(getString(R.string.progress_waiting));
                call.enqueue(new Callback<ServerResponse>() {
                    @Override
                    public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                        progress.dismiss();
                        ServerResponse serverResponse = response.body();
                        if (serverResponse.userId != null) {
                            // add User to database with the created UserID
                            User user = new User(name, email, password, serverResponse.userId);
                            database.addUser(user);
                            Toast.makeText(RegisterActivity.this, getString(R.string.success_register), Toast.LENGTH_LONG).show();
                            // open TrainingActivity to begin training the User
                            Intent intent = new Intent(RegisterActivity.this, TrainingActivity.class);
                            intent.putExtra("email", email);
                            startActivity(intent);
                        } else {
                            // notify user that a UserID was not returned
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this).setTitle("Error").setMessage(getString(R.string.no_user_id)).setPositiveButton("OK", null);
                            AlertDialog alert = builder.create();
                            alert.setCanceledOnTouchOutside(false);
                            alert.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServerResponse> call, Throwable t) {
                        progress.dismiss();
                        // notify user and exit
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this).setTitle("Error").setMessage(getString(R.string.no_response)).setPositiveButton("OK", null);
                        AlertDialog alert = builder.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.show();
                    }
                });
            } else {
                // notify user that email is taken
                inputEmail.setError(getString(R.string.existing_email));
                inputEmail.requestFocus();
            }
        }
    }
}