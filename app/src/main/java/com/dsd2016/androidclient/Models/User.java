package com.dsd2016.androidclient.Models;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private String email;
    private String password;
    private String userId;
    private List<Picture> pictures;

    /** Create a new User using the given information */
    public User(String name, String email, String password, String userId) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.userId = userId;
        this.pictures = new ArrayList<>();
    }

    /** Remove a Picture by its pictureId. */
    public void removePicture(int pictureId) {
        for (int i = 0; i < pictures.size(); i++) {
            if (pictures.get(i).getPictureId() == pictureId) {
                pictures.remove(i);
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }
}
