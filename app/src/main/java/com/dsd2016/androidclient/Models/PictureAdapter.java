package com.dsd2016.androidclient.Models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dsd2016.androidclient.R;

import java.util.List;

public class PictureAdapter extends BaseAdapter {
    Context context;
    List<Picture> pictures;
    private static LayoutInflater inflater;

    /** Assist the TrainingActivity in displaying Pictures alongside their path and pictureId. */
    public PictureAdapter(Context context, List<Picture> pictures) {
        this.context = context;
        this.pictures = pictures;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @Override
    public Object getItem(int position) {
        return pictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) vi = inflater.inflate(R.layout.list_item_picture, null);
        // add the Picture to the list
        ImageView imageView = (ImageView) vi.findViewById(R.id.listPictureView);
        Bitmap bitmap = BitmapFactory.decodeFile(pictures.get(position).getPath());
        imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth()*0.5), (int) (bitmap.getHeight()*0.5), false));
        // add the path and pictureId to the list
        TextView textView = (TextView) vi.findViewById(R.id.listPictureText);
        textView.setText("Filename: " + pictures.get(position).getPath() + "\nPictureID: " + pictures.get(position).getPictureId());
        return vi;
    }
}