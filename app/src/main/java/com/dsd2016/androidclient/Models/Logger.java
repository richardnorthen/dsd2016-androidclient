package com.dsd2016.androidclient.Models;

import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    public static String logText = "";
    public static void log(EditText area, String text) {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        logText += "\n" + timeStamp + ">" + text;
        area.setText(logText);
        int bottom = (area.getLineCount() -1) * area.getLineHeight();
        area.scrollTo(0, bottom);
    }
}
